<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Welcome!</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" href="/favicon.ico" />
    <meta name="robots" content="noindex" />
	<style type="text/css"><!--
    body {
        color: #444444;
        background-color: #EEEEEE;
        font-family: 'Trebuchet MS', sans-serif;
        font-size: 80%;
    }
    h1 {}
    h2 { font-size: 1.2em; }
    #page{
        background-color: #FFFFFF;
        width: 60%;
        margin: 24px auto;
        padding: 12px;
    }
    #header{
        padding: 6px ;
        text-align: center;
    }
    .header{ background-color: #83A342; color: #FFFFFF; }
    #content {
        padding: 4px 0 24px 0;
    }
    #footer {
        color: #666666;
        background: #f9f9f9;
        padding: 10px 20px;
        border-top: 5px #efefef solid;
        font-size: 0.8em;
        text-align: center;
    }
    #footer a {
        color: #999999;
    }
    --></style>
</head>
<body>

	<?php
		
		for($i=0;$i<=6;$i++) {
        if($i==2) continue;

        $ch = curl_init("https://mss.swicpc.bankgirot.se/swish-cpcapi/api/v1/paymentrequests");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLVERSION, $i);

        if(curl_exec($ch) === false)
                echo $i.': ' . curl_error($ch)."<br />";
        else
                echo $i.': works'."<br />";

        echo "\n";
        curl_close($ch);
}


		echo "<pre>";
print_r(curl_version());
echo "</pre>";
function curl($sslver, $ciphers) {
        $ch = curl_init("https://mss.swicpc.bankgirot.se/swish-cpcapi/api/v1/paymentrequests");
        curl_setopt($ch, CURLOPT_NOBODY, true);

        curl_setopt($ch, CURLOPT_SSLVERSION, $sslver);
        curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, $ciphers);

        $result = curl_exec($ch);
        curl_close($ch);
        return ($result === false) ? false : true;
}

$goodcurl = (curl_version()['version_number'] >= 467456); // curl >= 7.34

// 1.
if ( curl(0, "DEFAULT:!SSLv2:!SSLv3") ) {
        echo "connected via TLSv1.2\n";
        exit;
}
// 2.
if( curl(1, "HIGH:!aNULL:!eNULL:!EXPORT:!DSS:!DES:!RC4:!3DES:!MD5:!PSK") ) {
        if($goodcurl)   echo "connected via TLSv1, TLSv1.1 or TLSv1.2\n";
        else            echo "connected via TLSv1\n";
        exit;
}
// 3.
if( !$goodcurl && curl(0, "HIGH:!aNULL:!eNULL:!EXPORT:!DSS:!DES:!RC4:!3DES:!MD5:!PSK") ) {
        echo "certainly connected via SSLv3, maybe TLSv1.1. TLSv1.2 is very unlikely\n";
        exit;
}
// 4.
if( curl(0, "DEFAULT") ) {
        echo "connected via SSLv3 or with weak ciphers\n";
        exit;
}
echo "Better don't contact that server...";



	$url = "https://mss.swicpc.bankgirot.se/swish-cpcapi/api/v1/paymentrequests";
	

	$data = array(
		"payeePaymentReference" => "0123456789",
		"callbackUrl" => "http://test.se",
		"payerAlias" => "46735589270",
		"payeeAlias" => "1234760039",
		"amount" => "100",
		"currency" => "SEK",
		"message" => "Hej"
	);
	
	$data_string = json_encode($data);
 
	$response = post_https($url, $data_string);
	echo $response;
 
	function post_https($url, $data_string)
	{
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL , $url ); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_TIMEOUT , 30); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt ($ch, CURLOPT_SSLVERSION, 4);
		curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'RC4-SHA');
		curl_setopt($ch, CURLOPT_CAINFO, "swishtestcert.pem");
		curl_setopt($ch, CURLOPT_SSLCERT, "newfile.crt.pem");
		curl_setopt($ch, CURLOPT_SSLKEY, "newfile.key.pem");
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, 'swish');
		curl_setopt($ch, CURLOPT_SSLKEYPASSWD, 'swish');
		curl_setopt($ch, CURLOPT_USERAGENT , "" );
		curl_setopt($ch, CURLOPT_POST , 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS , $data_string ); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json',
		    'Content-Length: ' . strlen($data_string))
		);
 
		$xml_response = curl_exec($ch); 
 
		if (curl_errno($ch)) { 
			$error_message = curl_error($ch); 
			$error_no = curl_errno($ch);
 
			echo "error_message: " . $error_message . "<br>";
			echo "error_no: " . $error_no . "<br>";
		}
 
		curl_close($ch);
 
		return $xml_response;
		
	}	



  ?>
	<a href="swish://paymentrequest?token=f34DS34lfd0d03fdDselkfd3ffk21">Swish</a>
   </body>
</html>
