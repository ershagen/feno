<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Create Page Lines
    |--------------------------------------------------------------------------
    */
    'create'            => 'Skapa :type',
    'title'             => 'Titel',
    'titleplace'        => 'Skriv in en titel',
    'desc'              => 'Beskrivning',
    'descplace'         => 'Skriv in en beskrivning',
    'entries'           => ':type Innehåll',

    'listtype'          => 'Sortering',
    'listasc'           => 'Numeric Asc List',
    'listdesc'          => 'Numeric Desc List',
    'normallist'        => 'Normal',


    'option'            => 'Alternativ',
    'text'              => 'Text',
    'image'             => 'Bild',
    'video'             => 'Video',
    'embed'             => 'Iframe',

    'add'               => 'Lägg till :type',
    'edit'               => 'Ändra :type',

    'mored'             => 'Mer detaljer',
    'lessd'             => 'Mindre detaljer',

    'entry_title'       => 'Titel',
    'entry_titleop'     => 'Titel (frivilligt)',
    'entry_body'        => 'Add some text about this entry.',
    'entry_source'      => 'Källa (frivilligt)',
    'entry_embeddesc'   => 'Enter embed or iframe',
    'entry_addimage'    => 'Klicka för att ladda upp ett foto.',

    'preview'           => 'Preview Image',
    'pickpreview'       => 'Pick a preview image.',
    'categories'        => 'Kategori',

    'makepreview'       => 'Make preview image',


    'addnew'            => 'Lägg till nytt innehåll',
    'savec'             => 'Spara',
    'createp'           => 'Skapa',
    'cancel'            => 'Stäng',

    'videotips'            => 'Please paste url to above input. Supported sites: <b>youtube</b>, <b>facebook</b>, <b>dailymotion</b>, <b>vimeo</b>',

];
