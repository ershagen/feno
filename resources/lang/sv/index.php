<?php

return [

    /*
    |--------------------------------------------------------------------------
    | All Language Lines
    |--------------------------------------------------------------------------
    */
    //General
    'list'          => 'Lista',
    'new-s'         => 'Nyhet',
    'poll'          => 'Poll',
    'video'         => 'Video',
    'lists'         => 'Listor',
    'news'          => 'Nyheter',
    'polls'         => 'Pollar',
    'videos'        => 'Videos',
    'videonews'     => 'VideoNews',
    'posts'         => 'Inlägg',

    'latest'        => 'Senaste :type',
    'trendings'     => 'Fenomenalt',
    'mostrecent'    => 'Most Recent :type',
    'hot'           => 'Hot :type',
    'trend'         => 'Trending :type',
    'todayhit'      => 'Dagens Hit :type',
    'top'           => 'topp :type',
    'new'           => 'Ny :type',
    'total'         => 'Totalt :type',

    'connect'       => 'Connect Us',
    'emptyplace'    => 'Nothing to see here...',

    //header
    'sections'      => 'Sections',
    'search'        => 'Sök...',
    'create'        => 'Skapa',

    'myprofile'     => 'Profil',
    'draft'         => 'Draft Posts',
    'trash'         => 'Trash Posts',
    'settings'      => 'Settings',
    'adminp'        => 'Admin',
    'logout'        => 'Logga ut',
    'register'      => 'Skapa',
    'login'         => 'Logga in',


    // Post page
    'admintools'    => 'Admin verktyg',
    'ownertools'    => 'Owner Tools',

    'edit'          => 'Ändra',
    'waitapprove'   => 'Waiting for approval',
    'approve'       => 'Approve',

    'createdby'     => 'Skapad av :user',
    'postedon'      => 'Skriven för :time',
    'updatedon'     => 'Uppdaterades för :time',

    'conversations' => 'Kommentarer',
    'disqusconversations' => 'Disqus Conversations',
    'maylike'       => 'Du kanske också gillar',


    // User page
    'usern'         => 'Namn',
    'gender'        => 'Kön',
    'location'      => 'Location',
    'about'         => 'Om',
    'links'         => 'Länkar',
    'joinedat'      => 'Skapade sitt konto :time',

    'account'        => 'Konto',
    'details'        => 'Detaljer',
    'onlycgange'     => 'Only if you want to change',
    'fullname'       => 'Full name',
    'savesettings'   => 'Save Settings',


    'all'           => 'Alla',

    'today'         => "Dagens",
    'weekly'        => 'Veckovis',

    'username'              => "Användarnamn",
    'email'                 => 'E-post',
    'password'              => 'Lösenord',
    'remember'              => 'Kom ihåg mig',

    'terms'                 => 'Terms and Conditions',
    'termslink'             => 'Jag accepterar villkoren :url ',


    'Doyouhaveanaccount'    => 'Har du redan ett konto?',
    'youdonthaveanaccount'  => 'Har du inget konto?',
    'registerwithemail'     => 'Skapa ett konto med e-post',
    'loginwithemail'        => 'Logga in med e-post',

    'connectfacebok'        => 'Facebook',
    'connectgoogle'         => 'Google',
    'connecttwitter'        => 'Twitter',

    'adminnote'             => 'You can change this settings because you are an admin',

    'ccommunity'            => 'Gilla oss!',
    'likeonface'            => 'Gilla oss på Facebook',
    'followontwitter'       => 'Follow Us On Twitter',
    'followongoogle'        => 'Follow Us On Google+',
    'followoninstagram'     => 'Follow Us On Instagram',

    'sharefacebook'         => 'Dela på Facebook',
    'sharetweet'            => 'Dela på Twitter',
    'sharegoogle'           => 'Google+',
    'sharereddit'           => 'Reddit',
    'sharepinterest'        => 'Pinterest',


    'nopermission'          => 'You have no permission for this!',

    'successcreated'        => 'Successfully Created! Your post will show up after approval',
    'successupdated'        => 'Your post is updated',

    'minthreeca'            => 'Min 2 character required!',




];
